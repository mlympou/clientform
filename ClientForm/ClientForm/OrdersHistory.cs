﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientForm
{
    public partial class OrdersHistory : Form
    {
        public OrdersHistory()
        {
            InitializeComponent();
        }

        private void OrdersHistory_Load(object sender, EventArgs e)
        {
            using (GoodysEntities db = new GoodysEntities())
            {
                var list = db.Orders;

                orderBindingSource.DataSource = list.ToList();
            }
        }
    }
}
