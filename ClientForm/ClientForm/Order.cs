//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientForm
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        public int Id { get; set; }
        public string Customer { get; set; }
        public string Cart { get; set; }
        public string Address { get; set; }
        public string Notes { get; set; }
        public Nullable<bool> Confirmed { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<bool> Payed { get; set; }
        public Nullable<Guid> TransactionId { get; set; }
        public decimal Price { get; set; }
    }
}
