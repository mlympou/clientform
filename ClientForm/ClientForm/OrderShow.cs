﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClientForm.Models;
using Newtonsoft.Json;

namespace ClientForm
{
    public partial class OrderShow : Form
    {
        public Order _order;
        public Customer _customer;
        public Main _main = null;
        Info.Address _address;
        Info.Cart _cart;

        public OrderShow(Order ord, Main mainForm)
        {
            _main = mainForm as Main;
            InitializeComponent();

            // Order data
            _order = ord;

            // Credit card 
            if (ord.Payed != null && (bool)ord.Payed)
            {
                creditCard_img.Visible = true;
                card_lbl.Visible = true;
            }

            // Address
            _address = JsonConvert.DeserializeObject<Info.Address>(_order.Address);

            // Customer
            _customer = Dataprovider.GetCustomerById(int.Parse(_order.Customer));

            // Items 
            _cart = JsonConvert.DeserializeObject<Info.Cart>(ord.Cart);

            // Address info
            address_val.Text = $"{_address.AddressName} {_address.AddressNumber}";
            region_val.Text = $"{_address.Region}";
            bell_val.Text = _address.NameOnPhone;
            Phone_val.Text = _customer.Phones;
            Notes_txb.Text = _order.Notes;
            floor_val.Text = _address.Floor;

            // Datagrid
            DataGridViewRow row;
            int idx = 0;
            decimal sum = 0;
            foreach (var item in _cart.Items)
            {
                row = (DataGridViewRow)Items_GridView.Rows[idx].Clone();
                row.Cells[0].Value = $"{item.Quantity}";
                row.Cells[1].Value = $"{item.Name}";
                row.Cells[1].Style.BackColor = Color.Coral;
                row.Cells[2].Value = $"{item.Price}";
                Items_GridView.Rows.Add(row);
                idx++;
                if (item.DealProducts.Count > 0)
                {
                    foreach (var dealproduct in item.DealProducts)
                    {
                        var sub = dealproduct.Split(',');
                        int c = idx;
                        foreach (var s in sub)
                        {
                            row = (DataGridViewRow)Items_GridView.Rows[idx].Clone();
                            row.Cells[0].Value = $"";
                            row.Cells[1].Value = $"{s}";
                            //if (idx == c)
                            //row.Cells[1].Style.BackColor = Color.Aqua;
                            Items_GridView.Rows.Add(row);
                            idx++;
                        }
                    }
                }
                foreach (var no in item.NoIncredients)
                {
                    row = (DataGridViewRow)Items_GridView.Rows[idx].Clone();
                    row.Cells[0].Value = $"";
                    row.Cells[1].Value = $"Χωρίς {no.Name}";
                    Items_GridView.Rows.Add(row);
                    idx++;
                }
                foreach (var extra in item.Extras)
                {
                    row = (DataGridViewRow)Items_GridView.Rows[idx].Clone();
                    row.Cells[0].Value = $"";
                    row.Cells[1].Value = $"Extra {extra.Name}";
                    Items_GridView.Rows.Add(row);
                    idx++;
                }

                // sum
                sum += item.Quantity * item.Price;
            }

            // synolo
            row = (DataGridViewRow)Items_GridView.Rows[idx].Clone();
            row.Cells[0].Value = $"Συν.";
            row.Cells[1].Value = $"";
            row.Cells[2].Value = $"{_order.Price}";
            Items_GridView.Rows.Add(row);
        }

        private void ConfirmOrder_Click(object sender, EventArgs e)
        {
            // confirm order 
            bool confirmed = Dataprovider.ConfirmOrder(_order.Id);

            if (confirmed)
            {
                // items in list remove 
                string[] data = new string[] { _order.Id.ToString(), $"{_address.AddressName} {_address.AddressNumber}, {_address.Region}" };
                _main.OrdersPending.Items.Remove(string.Join(":", data));

                // Print Order
                Print();

                // close form 
                Hide();
            }

            // close form 
        }

        Bitmap bmp;
        private void Print()
        {
            //if (printPreviewDialog1.ShowDialog() == DialogResult.OK)
            printDocument1.Print();
        }

        private void PrintDocument1_PrintPage_1(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string address = $"{_address.AddressName} {_address.AddressNumber}";
            string addressNotes = _address.Notes == null ? "" : _address.Notes;
            string region = $"{_address.Region}";
            string bell = _address.NameOnPhone;
            string Phone = _customer.Phones;
            string Notes = _order.Notes;
            string floor = _address.Floor;

            var fnt = new Font("Times new Roman", 14, FontStyle.Bold);
            int x = 0, y = 0;
            int dy = (int)fnt.GetHeight(e.Graphics) * 1; //change this factor to control line spacing

            // geuseis logo
            e.Graphics.DrawString("Γυράδικο Γεύσεις όπως παλιά", fnt, Brushes.Black, new PointF(x, y)); y += dy;

            // time
            var date = DateTime.Now;
            e.Graphics.DrawString(date.ToString(), fnt, Brushes.Black, new PointF(x, y)); y += dy;

            var addressArray = Split($"Διεύθυνση:  {address}", 30);
            foreach (var ad in addressArray)
            {
                e.Graphics.DrawString(ad, fnt, Brushes.Black, new PointF(x, y)); y += dy;
            }

            if (!string.IsNullOrEmpty(addressNotes))
            {
                var addressNotesArray = Split($"Οδηγίες:  {addressNotes}", 30);
                foreach (var ad in addressNotesArray)
                {
                    e.Graphics.DrawString(ad, fnt, Brushes.Black, new PointF(x, y)); y += dy;
                }
            }

            var regionArray = Split($"Περιοχή:  {region}", 30);
            foreach (var r in regionArray)
            {
                e.Graphics.DrawString(r, fnt, Brushes.Black, new PointF(x, y)); y += dy;
            }

            var nameArray = Split($"Κουδούνι:   {bell}", 30);
            foreach (var n in nameArray)
            {
                e.Graphics.DrawString(n, fnt, Brushes.Black, new PointF(x, y)); y += dy;
            }

            var phoneArray = Split($"Τηλέφωνο:   {Phone}", 30);
            foreach (var ph in phoneArray)
            {
                e.Graphics.DrawString(ph, fnt, Brushes.Black, new PointF(x, y)); y += dy;
            }

            var notesArray = Split($"Σημειώσεις: {Notes}", 30);
            foreach (var n in notesArray)
            {
                e.Graphics.DrawString(n, fnt, Brushes.Black, new PointF(x, y)); y += dy;
            }

            var floorArray = Split($"Όροφος:     {floor}", 30);
            foreach (var f in floorArray)
            {
                e.Graphics.DrawString(f, fnt, Brushes.Black, new PointF(x, y)); y += dy;
            }

            e.Graphics.DrawString($"------------------------------", fnt, Brushes.Black, new PointF(x, y)); y += dy;

            // Ορδερ
            // Datagrid            
            int idx = 0;
            decimal sum = 0;
            foreach (var item in _cart.Items)
            {
                var itemArray = Split($"{item.Quantity} {item.Name}  {item.Price}", 30);
                foreach (var it in itemArray)
                {
                    e.Graphics.DrawString(it, fnt, Brushes.Black, new PointF(x, y)); y += dy;
                }

                if (item.DealProducts.Count > 0)
                {
                    foreach (var dealproduct in item.DealProducts)
                    {
                        var sub = dealproduct.Split(',');
                        int c = idx;
                        foreach (var s in sub)
                        {
                            e.Graphics.DrawString($"{s}", fnt, Brushes.Black, new PointF(x, y)); y += dy;
                        }
                    }
                }
                foreach (var no in item.NoIncredients)
                {
                    e.Graphics.DrawString($"Χωρίς {no.Name}", fnt, Brushes.Black, new PointF(x, y)); y += dy;
                }
                foreach (var extra in item.Extras)
                {
                    e.Graphics.DrawString($"Extra {extra.Name}", fnt, Brushes.Black, new PointF(x, y)); y += dy;
                }

                // sum
                sum += item.Quantity * item.Price;
            }

            // synolo
            e.Graphics.DrawString($"Σύνολο {_order.Price}", fnt, Brushes.Black, new PointF(x, y)); y += dy;

            // card payed
            if (_order.Payed != null && _order.Payed == true)
                e.Graphics.DrawString($"Η παραγγελεία έχει πληρωθεί!!!", fnt, Brushes.Black, new PointF(x, y)); y += dy;

        }

        static List<string> Split1(string str, int chunkSize)
        {
            return str.Length < chunkSize ? new List<string> { str } : Enumerable.Range(0, str.Length / chunkSize + 1)
                .Select(i => str.Substring(i * chunkSize, chunkSize)).ToList();
        }

        IEnumerable<string> Split(string stringToSplit, int maximumLineLength)
        {
            var words = stringToSplit.Split(' ').Concat(new[] { "" });
            return
                words
                    .Skip(1)
                    .Aggregate(
                        words.Take(1).ToList(),
                        (a, w) =>
                        {
                            var last = a.Last();
                            while (last.Length > maximumLineLength)
                            {
                                a[a.Count() - 1] = last.Substring(0, maximumLineLength);
                                last = last.Substring(maximumLineLength);
                                a.Add(last);
                            }
                            var test = last + " " + w;
                            if (test.Length > maximumLineLength)
                            {
                                a.Add(w);
                            }
                            else
                            {
                                a[a.Count() - 1] = test;
                            }
                            return a;
                        });
        }

        private void TenMinute_btn_Click(object sender, EventArgs e)
        {
            Confirm(10);
        }

        private void Confirm(int time)
        {
            // confirm order 
            bool confirmed = Dataprovider.ConfirmOrder(_order.Id);

            if (confirmed)
            {
                // items in list remove 
                string[] data = new string[] { _order.Id.ToString(), $"{_address.AddressName} {_address.AddressNumber}, {_address.Region}" };
                _main.OrdersPending.Items.Remove(string.Join(":", data));

                // Print Order
                Print();

                // Email customer
                SendConfirmEmail(time, _order, _address, _customer, _cart);

                // close form 
                Hide();
            }
        }

        private void TewntyMinute_btn_Click(object sender, EventArgs e)
        {
            Confirm(20);
        }

        private void ThirtyMinute_btn_Click(object sender, EventArgs e)
        {
            Confirm(30);
        }

        private void FourteenMinute_btn_Click(object sender, EventArgs e)
        {
            Confirm(45);
        }

        private void OneHour_btn_Click(object sender, EventArgs e)
        {
            Confirm(60);
        }

        private static void SendConfirmEmail(int time, Order order, Info.Address address, Customer customer,Info.Cart cart)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient("mail.geyseis.gr", 8889);

                message.From = new MailAddress("info@geyseis.gr", "Γυράδικο Γεύσεις");
                message.To.Add(new MailAddress(customer.UserName));
                message.Subject = $"Επιβεβαίωση παραγγελείας #{order.Id}";
                message.BodyEncoding = System.Text.Encoding.GetEncoding("UTF-8");
                message.IsBodyHtml = true;

                message.Body = $"<a>Καλησπέρα {customer.Name} {customer.Surname},</a><br><br> \n";                
                message.Body += $"<a>Έχουμε λάβει την παραγγελεία σας θα είναι στην πόρτα σας σε περίπου {time} λεπτά!</a><br><br>\n";

                // address
                message.Body += $"<a>Παράδοση στη διεύθυνση: {address.AddressName} {address.AddressNumber}, {address.Region}</a><br>";
                message.Body += $"<a>Όνομα στο κουδούνι: {address.NameOnPhone}</a><br><br>";

                // cart
                message.Body += "<table border='1'>";
                foreach (var item in cart.Items)
                {
                    // new row
                    message.Body += "<tr>";
                    // new cell
                    message.Body += $"<td>{item.Quantity}</td>";
                    message.Body += $"<td>{item.Name}</td>";
                    message.Body += $"<td>{item.Price}</td>";
                    message.Body += "</tr>";

                    if (item.DealProducts.Count > 0)
                    {
                        foreach (var dealproduct in item.DealProducts)
                        {
                            var sub = dealproduct.Split(',');                            
                            foreach (var s in sub)
                            {
                                // new row
                                message.Body += "<tr>";
                                message.Body += $"<td></td>";
                                message.Body += $"<td>{s}</td>";
                                message.Body += "</tr>";
                            }
                        }
                    }
                    foreach (var no in item.NoIncredients)
                    {
                        message.Body += "<tr>";
                        message.Body += $"<td></td>";
                        message.Body += $"<td>Χωρίς {no.Name}</td>";
                        message.Body += "</tr>";
                    }
                    foreach (var extra in item.Extras)
                    {
                        message.Body += "<tr>";
                        message.Body += $"<td></td>";
                        message.Body += $"<td>Extra {extra.Name}</td>";
                        message.Body += "</tr>";
                    }                    
                }                
                message.Body += "</table><br>";

                // find if discount made 
                var dealProds = cart.Items.Where(p => p.DealProducts.Count > 0).Sum(it => it.Price * it.Quantity);
                var totalPriceNoDiscount = cart.Items.Where(p => p.DealProducts.Count == 0).Sum(it => it.Price * it.Quantity) + dealProds;
                bool discount = totalPriceNoDiscount >= 10 ? true : false;
                message.Body += discount ? $"<a>Σύνολο με έκπτωση 10%: {order.Price}€</a><br>" : $"<a>Σύνολο: {order.Price}€</a><br>";
                if (order.Payed != null && order.Payed == true)
                    message.Body += $"<strong>Η παραγγελεία σας έχει πληρωθεί με κάρτα!!!</strong><br>";
                message.Body += $"<a>Ευχαριστούμε πολύ!</a>\n";


                //smtp.Port = 465;
                //smtp.Host = "mail.geyseis.gr";
                //smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("info@geyseis.gr", "Leon1234*");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"err: {ex.Message}");
            }
        }
    }
}
