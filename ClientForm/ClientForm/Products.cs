﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientForm
{
    public partial class Products : Form
    {
        public Products()
        {
            InitializeComponent();
        }

        private void Products_Load(object sender, EventArgs e)
        {
            using (GoodysEntities db = new GoodysEntities())
            {
                var products = db.Products.ToList();

                // Gyros kotopoulo
                if (!(bool)products.Where(p => p.Id == 2).FirstOrDefault().Available)
                {
                    gyros_kota_av.BackColor = Color.Red;
                }
                else
                {
                    gyros_kota_av.BackColor = Color.Green;
                }

                // Gyros xoirinos
                if (!(bool)products.Where(p => p.Id == 1).FirstOrDefault().Available)
                {
                    gyros_xoir_av.BackColor = Color.Red;
                }
                else
                {
                    gyros_xoir_av.BackColor = Color.Green;
                }
            }
        }

        private void Gyros_xoir_av_Click(object sender, EventArgs e)
        {
            // set all gyros products unavailable
            // 1 sandwitch
            // 10 club
            // 39
            List<int> ProductIds = new List<int> { 1, 10, 39 };
            bool available = gyros_xoir_av.BackColor == Color.Red ? true : false;

            if (Models.Dataprovider.UpdateProducts(ProductIds, available))
            {
                // succes
                gyros_xoir_av.BackColor = available ? Color.Green : Color.Red;
            }
            else
            {
                MessageBox.Show("Κάτι πήγε στραβά!!");
            }
        }

        private void Gyros_kota_av_Click(object sender, EventArgs e)
        {
            List<int> ProductIds = new List<int> { 2, 11, 40 };
            bool available = gyros_kota_av.BackColor == Color.Red ? true : false;

            if (Models.Dataprovider.UpdateProducts(ProductIds, available))
            {
                // succes
                gyros_kota_av.BackColor = available ? Color.Green : Color.Red;
            }
            else
            {
                MessageBox.Show("Κάτι πήγε στραβά!!");
            }
        }

        //static async void UpdateAvailibillityAsync(int id, bool available)
        //{
        //    await Task.Run(() => Models.Dataprovider.UpdateAvailabillity(id, available));            
        //}

        //private void ProductDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        //{
        //    // update db 
        //    if (productDataGridView.CurrentRow == null)
        //        return;
        //    int id = (int)productDataGridView.CurrentRow.Cells["Id"].Value;
        //    bool available = (bool)productDataGridView.CurrentRow.Cells["Available"].Value;

        //    // async update db 
        //    UpdateAvailibillityAsync(id, available);
        //}

        //private void SaveToolStripButton_Click(object sender, EventArgs e)
        //{
        //    SendKeys.Send("{TAB}");
        //}

        //private void Cat1_Click(object sender, EventArgs e)
        //{
        //    // Sandwitch 1
        //    var products = Models.Dataprovider.GetProducts();
        //    productBindingSource.DataSource = (from p in products.Where(c => c.CategoryId == 1) select p);
        //}

        //private void Merides_Click(object sender, EventArgs e)
        //{
        //    // Merides = 6
        //    var products = Models.Dataprovider.GetProducts();
        //    productBindingSource.DataSource = (from p in products.Where(c => c.CategoryId == 6) select p);
        //}

        //private void Club_Click(object sender, EventArgs e)
        //{
        //    // Club = 3
        //    var products = Models.Dataprovider.GetProducts();
        //    productBindingSource.DataSource = (from p in products.Where(c => c.CategoryId == 3) select p);
        //}

        //private void All_Click(object sender, EventArgs e)
        //{
        //    var products = Models.Dataprovider.GetProducts();
        //    productBindingSource.DataSource = (from p in products select p);
        //}
    }
}
