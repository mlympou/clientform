//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientForm
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product
    {
        public int Id { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string Incredients { get; set; }
        public string Name { get; set; }
        public string Img { get; set; }
        public Nullable<bool> isNew { get; set; }
        public string SubProducts { get; set; }
        public Nullable<bool> Available { get; set; }
    }
}
