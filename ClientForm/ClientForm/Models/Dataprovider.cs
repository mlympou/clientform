﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientForm.Models
{
    class Dataprovider
    {

        public static List<Order> GetUnconfirmedOrders()
        {
            try
            {
                using (GoodysEntities Db = new GoodysEntities())
                {
                    return (from orders in Db.Orders where (bool)orders.Confirmed == false select orders).ToList();
                }
            }
            catch
            {
                return new List<Order> { };
            }
        }

        public static Order GetOrderById(int orderId)
        {
            using (GoodysEntities Db = new GoodysEntities())
            {
                return (from order in Db.Orders where orderId == order.Id select order).FirstOrDefault();
            }            
        }

        public static Customer GetCustomerById(int CustomerId)
        {
            using (GoodysEntities Db = new GoodysEntities())
            {
                return (from cust in Db.Customers where CustomerId == cust.Id select cust).FirstOrDefault();
            }            
        }

        public static bool ConfirmOrder(int orderId)
        {
            try
            {
                using (GoodysEntities Db = new GoodysEntities())
                {
                    var order = (from ord in Db.Orders where orderId == ord.Id select ord).FirstOrDefault();
                    order.Confirmed = true;
                    Db.SaveChanges();
                    return true;
                }                
            }
            catch
            {
                return false;
            }
        }

        internal static bool UpdateProducts(List<int> productIds, bool available)
        {
            try
            {
                using (GoodysEntities db = new GoodysEntities())
                {
                   foreach (var id in productIds)
                    {
                        var row = db.Products.SingleOrDefault(p => p.Id == id);
                        if (row != null)
                        {
                            row.Available = available;
                        }
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static List<Product> GetProducts()
        {
            using (GoodysEntities db = new GoodysEntities())
            {
                return db.Products.ToList();
            }
        }

        public static void UpdateAvailabillity(int productId, bool available)
        {
            using (GoodysEntities db = new GoodysEntities())
            {
                var product = db.Products.Where(i => i.Id == productId).FirstOrDefault();
                product.Available = available;
                db.SaveChanges();
            }
        }
    }
}
