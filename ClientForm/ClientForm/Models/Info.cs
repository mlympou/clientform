﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientForm.Models
{
    class Info
    {
        public class Address
        {
            public string Region { set; get; }
            public string AddressName { set; get; }
            public string AddressNumber { set; get; }
            public string Floor { set; get; }
            public string NameOnPhone { set; get; }
            public string Notes { set; get; }
            public string Id { set; get; }
        }

        public class Cart
        {
            public List<CartItem> Items { set; get; }

            public Cart()
            {
                Items = new List<CartItem> { };
            }
        }

        public class CartItem
        {
            public string Name { set; get; }
            public int Id { set; get; }
            public int Index { set; get; }
            public decimal Price { set; get; }
            public int Quantity { set; get; }
            public List<ProductExtra> Extras { set; get; }
            public List<Incredient> NoIncredients { set; get; }
            public List<string> DealProducts { set; get; }

            public CartItem()
            {
                Extras = new List<ProductExtra> { };
                NoIncredients = new List<Incredient> { };
                DealProducts = new List<string> { };
            }
        }

        public class SubProduct
        {
            public int id { set; get; }
            public string price { set; get; }
            public string name { set; get; }
            public string description { set; get; }
        }
    }
}
