﻿namespace ClientForm
{
    partial class OrderShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderShow));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Phone_val = new System.Windows.Forms.Label();
            this.bell_val = new System.Windows.Forms.Label();
            this.region_val = new System.Windows.Forms.Label();
            this.address_val = new System.Windows.Forms.Label();
            this.region_lbl = new System.Windows.Forms.Label();
            this.bell_lbl = new System.Windows.Forms.Label();
            this.Phone_lbl = new System.Windows.Forms.Label();
            this.address_lbl = new System.Windows.Forms.Label();
            this.Notes_lbl = new System.Windows.Forms.Label();
            this.Notes_txb = new System.Windows.Forms.TextBox();
            this.Floor_lbl = new System.Windows.Forms.Label();
            this.floor_val = new System.Windows.Forms.Label();
            this.Items_GridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.creditCard_img = new System.Windows.Forms.PictureBox();
            this.card_lbl = new System.Windows.Forms.Label();
            this.TenMinute_btn = new System.Windows.Forms.Button();
            this.TewntyMinute_btn = new System.Windows.Forms.Button();
            this.ThirtyMinute_btn = new System.Windows.Forms.Button();
            this.FourteenMinute_btn = new System.Windows.Forms.Button();
            this.OneHour_btn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Items_GridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.creditCard_img)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.Phone_val, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.bell_val, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.region_val, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.address_val, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.region_lbl, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.bell_lbl, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Phone_lbl, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.address_lbl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Notes_lbl, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.Notes_txb, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.Floor_lbl, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.floor_val, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(139, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 167);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Phone_val
            // 
            this.Phone_val.AutoSize = true;
            this.Phone_val.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone_val.Location = new System.Drawing.Point(325, 84);
            this.Phone_val.Name = "Phone_val";
            this.Phone_val.Size = new System.Drawing.Size(60, 24);
            this.Phone_val.TabIndex = 8;
            this.Phone_val.Text = "label1";
            // 
            // bell_val
            // 
            this.bell_val.AutoSize = true;
            this.bell_val.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bell_val.Location = new System.Drawing.Point(325, 57);
            this.bell_val.Name = "bell_val";
            this.bell_val.Size = new System.Drawing.Size(60, 24);
            this.bell_val.TabIndex = 7;
            this.bell_val.Text = "label1";
            // 
            // region_val
            // 
            this.region_val.AutoSize = true;
            this.region_val.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.region_val.Location = new System.Drawing.Point(325, 30);
            this.region_val.Name = "region_val";
            this.region_val.Size = new System.Drawing.Size(60, 24);
            this.region_val.TabIndex = 6;
            this.region_val.Text = "label1";
            // 
            // address_val
            // 
            this.address_val.AutoSize = true;
            this.address_val.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address_val.Location = new System.Drawing.Point(325, 3);
            this.address_val.Name = "address_val";
            this.address_val.Size = new System.Drawing.Size(60, 24);
            this.address_val.TabIndex = 5;
            this.address_val.Text = "label1";
            // 
            // region_lbl
            // 
            this.region_lbl.AutoSize = true;
            this.region_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.region_lbl.Location = new System.Drawing.Point(6, 30);
            this.region_lbl.Name = "region_lbl";
            this.region_lbl.Size = new System.Drawing.Size(83, 24);
            this.region_lbl.TabIndex = 1;
            this.region_lbl.Text = "Περιοχή";
            // 
            // bell_lbl
            // 
            this.bell_lbl.AutoSize = true;
            this.bell_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bell_lbl.Location = new System.Drawing.Point(6, 57);
            this.bell_lbl.Name = "bell_lbl";
            this.bell_lbl.Size = new System.Drawing.Size(93, 24);
            this.bell_lbl.TabIndex = 2;
            this.bell_lbl.Text = "Κουδούνι";
            // 
            // Phone_lbl
            // 
            this.Phone_lbl.AutoSize = true;
            this.Phone_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Phone_lbl.Location = new System.Drawing.Point(6, 84);
            this.Phone_lbl.Name = "Phone_lbl";
            this.Phone_lbl.Size = new System.Drawing.Size(101, 24);
            this.Phone_lbl.TabIndex = 3;
            this.Phone_lbl.Text = "Τηλέφωνο";
            // 
            // address_lbl
            // 
            this.address_lbl.AutoSize = true;
            this.address_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address_lbl.Location = new System.Drawing.Point(6, 3);
            this.address_lbl.Name = "address_lbl";
            this.address_lbl.Size = new System.Drawing.Size(104, 24);
            this.address_lbl.TabIndex = 0;
            this.address_lbl.Text = "Διεύθυνση";
            // 
            // Notes_lbl
            // 
            this.Notes_lbl.AutoSize = true;
            this.Notes_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Notes_lbl.Location = new System.Drawing.Point(6, 138);
            this.Notes_lbl.Name = "Notes_lbl";
            this.Notes_lbl.Size = new System.Drawing.Size(69, 24);
            this.Notes_lbl.TabIndex = 9;
            this.Notes_lbl.Text = "Σχόλια";
            // 
            // Notes_txb
            // 
            this.Notes_txb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Notes_txb.Location = new System.Drawing.Point(325, 141);
            this.Notes_txb.Multiline = true;
            this.Notes_txb.Name = "Notes_txb";
            this.Notes_txb.Size = new System.Drawing.Size(469, 20);
            this.Notes_txb.TabIndex = 10;
            // 
            // Floor_lbl
            // 
            this.Floor_lbl.AutoSize = true;
            this.Floor_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Floor_lbl.Location = new System.Drawing.Point(6, 111);
            this.Floor_lbl.Name = "Floor_lbl";
            this.Floor_lbl.Size = new System.Drawing.Size(83, 24);
            this.Floor_lbl.TabIndex = 11;
            this.Floor_lbl.Text = "Όροφος";
            // 
            // floor_val
            // 
            this.floor_val.AutoSize = true;
            this.floor_val.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.floor_val.Location = new System.Drawing.Point(325, 111);
            this.floor_val.Name = "floor_val";
            this.floor_val.Size = new System.Drawing.Size(60, 24);
            this.floor_val.TabIndex = 12;
            this.floor_val.Text = "label1";
            // 
            // Items_GridView
            // 
            this.Items_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Items_GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Price});
            this.Items_GridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Items_GridView.Location = new System.Drawing.Point(3, 25);
            this.Items_GridView.Name = "Items_GridView";
            this.Items_GridView.Size = new System.Drawing.Size(865, 316);
            this.Items_GridView.TabIndex = 2;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.HeaderText = "#";
            this.Column1.Name = "Column1";
            this.Column1.Width = 45;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "Προϊόν";
            this.Column2.Name = "Column2";
            // 
            // Price
            // 
            this.Price.HeaderText = "Τιμή Μον.";
            this.Price.Name = "Price";
            this.Price.Width = 120;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Items_GridView);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(106, 185);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(871, 344);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Παραγγελεία";
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage_1);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // creditCard_img
            // 
            this.creditCard_img.Image = ((System.Drawing.Image)(resources.GetObject("creditCard_img.Image")));
            this.creditCard_img.Location = new System.Drawing.Point(12, 15);
            this.creditCard_img.Name = "creditCard_img";
            this.creditCard_img.Size = new System.Drawing.Size(110, 105);
            this.creditCard_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.creditCard_img.TabIndex = 4;
            this.creditCard_img.TabStop = false;
            this.creditCard_img.Visible = false;
            // 
            // card_lbl
            // 
            this.card_lbl.AutoSize = true;
            this.card_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.card_lbl.ForeColor = System.Drawing.Color.ForestGreen;
            this.card_lbl.Location = new System.Drawing.Point(12, 108);
            this.card_lbl.Name = "card_lbl";
            this.card_lbl.Size = new System.Drawing.Size(121, 24);
            this.card_lbl.TabIndex = 5;
            this.card_lbl.Text = "Πληρωμένη";
            this.card_lbl.Visible = false;
            // 
            // TenMinute_btn
            // 
            this.TenMinute_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.TenMinute_btn.Location = new System.Drawing.Point(149, 549);
            this.TenMinute_btn.Name = "TenMinute_btn";
            this.TenMinute_btn.Size = new System.Drawing.Size(80, 50);
            this.TenMinute_btn.TabIndex = 6;
            this.TenMinute_btn.Text = "10\'";
            this.TenMinute_btn.UseVisualStyleBackColor = false;
            this.TenMinute_btn.Click += new System.EventHandler(this.TenMinute_btn_Click);
            // 
            // TewntyMinute_btn
            // 
            this.TewntyMinute_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.TewntyMinute_btn.Location = new System.Drawing.Point(325, 549);
            this.TewntyMinute_btn.Name = "TewntyMinute_btn";
            this.TewntyMinute_btn.Size = new System.Drawing.Size(80, 50);
            this.TewntyMinute_btn.TabIndex = 7;
            this.TewntyMinute_btn.Text = "20\'";
            this.TewntyMinute_btn.UseVisualStyleBackColor = false;
            this.TewntyMinute_btn.Click += new System.EventHandler(this.TewntyMinute_btn_Click);
            // 
            // ThirtyMinute_btn
            // 
            this.ThirtyMinute_btn.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ThirtyMinute_btn.Location = new System.Drawing.Point(501, 549);
            this.ThirtyMinute_btn.Name = "ThirtyMinute_btn";
            this.ThirtyMinute_btn.Size = new System.Drawing.Size(80, 50);
            this.ThirtyMinute_btn.TabIndex = 8;
            this.ThirtyMinute_btn.Text = "30\'";
            this.ThirtyMinute_btn.UseVisualStyleBackColor = false;
            this.ThirtyMinute_btn.Click += new System.EventHandler(this.ThirtyMinute_btn_Click);
            // 
            // FourteenMinute_btn
            // 
            this.FourteenMinute_btn.BackColor = System.Drawing.SystemColors.Highlight;
            this.FourteenMinute_btn.Location = new System.Drawing.Point(677, 549);
            this.FourteenMinute_btn.Name = "FourteenMinute_btn";
            this.FourteenMinute_btn.Size = new System.Drawing.Size(80, 50);
            this.FourteenMinute_btn.TabIndex = 9;
            this.FourteenMinute_btn.Text = "45\'";
            this.FourteenMinute_btn.UseVisualStyleBackColor = false;
            this.FourteenMinute_btn.Click += new System.EventHandler(this.FourteenMinute_btn_Click);
            // 
            // OneHour_btn
            // 
            this.OneHour_btn.BackColor = System.Drawing.Color.OrangeRed;
            this.OneHour_btn.Location = new System.Drawing.Point(853, 549);
            this.OneHour_btn.Name = "OneHour_btn";
            this.OneHour_btn.Size = new System.Drawing.Size(80, 50);
            this.OneHour_btn.TabIndex = 10;
            this.OneHour_btn.Text = "60\'";
            this.OneHour_btn.UseVisualStyleBackColor = false;
            this.OneHour_btn.Click += new System.EventHandler(this.OneHour_btn_Click);
            // 
            // OrderShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 611);
            this.Controls.Add(this.OneHour_btn);
            this.Controls.Add(this.FourteenMinute_btn);
            this.Controls.Add(this.ThirtyMinute_btn);
            this.Controls.Add(this.TewntyMinute_btn);
            this.Controls.Add(this.TenMinute_btn);
            this.Controls.Add(this.card_lbl);
            this.Controls.Add(this.creditCard_img);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "OrderShow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OrderShow";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Items_GridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.creditCard_img)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label address_lbl;
        private System.Windows.Forms.Label region_lbl;
        private System.Windows.Forms.Label bell_lbl;
        private System.Windows.Forms.Label Phone_lbl;
        private System.Windows.Forms.Label Phone_val;
        private System.Windows.Forms.Label bell_val;
        private System.Windows.Forms.Label region_val;
        private System.Windows.Forms.Label address_val;
        private System.Windows.Forms.DataGridView Items_GridView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Label Notes_lbl;
        private System.Windows.Forms.TextBox Notes_txb;
        private System.Windows.Forms.Label Floor_lbl;
        private System.Windows.Forms.Label floor_val;
        private System.Windows.Forms.PictureBox creditCard_img;
        private System.Windows.Forms.Label card_lbl;
        private System.Windows.Forms.Button TenMinute_btn;
        private System.Windows.Forms.Button TewntyMinute_btn;
        private System.Windows.Forms.Button ThirtyMinute_btn;
        private System.Windows.Forms.Button FourteenMinute_btn;
        private System.Windows.Forms.Button OneHour_btn;
    }
}