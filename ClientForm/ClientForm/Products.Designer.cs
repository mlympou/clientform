﻿namespace ClientForm
{
    partial class Products
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.gyros_xoir_av = new System.Windows.Forms.Button();
            this.gyros_kota_av = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.gyros_xoir_av, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gyros_kota_av, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(941, 545);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // gyros_xoir_av
            // 
            this.gyros_xoir_av.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gyros_xoir_av.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.gyros_xoir_av.Location = new System.Drawing.Point(3, 3);
            this.gyros_xoir_av.Name = "gyros_xoir_av";
            this.gyros_xoir_av.Size = new System.Drawing.Size(182, 130);
            this.gyros_xoir_av.TabIndex = 0;
            this.gyros_xoir_av.Text = "Γύρος Χοιρινός";
            this.gyros_xoir_av.UseVisualStyleBackColor = true;
            this.gyros_xoir_av.Click += new System.EventHandler(this.Gyros_xoir_av_Click);
            // 
            // gyros_kota_av
            // 
            this.gyros_kota_av.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gyros_kota_av.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.gyros_kota_av.Location = new System.Drawing.Point(191, 3);
            this.gyros_kota_av.Name = "gyros_kota_av";
            this.gyros_kota_av.Size = new System.Drawing.Size(182, 130);
            this.gyros_kota_av.TabIndex = 1;
            this.gyros_kota_av.Text = "Γύρος Κοτόπουλο";
            this.gyros_kota_av.UseVisualStyleBackColor = true;
            this.gyros_kota_av.Click += new System.EventHandler(this.Gyros_kota_av_Click);
            // 
            // Products
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 545);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Products";
            this.Text = "Products";
            this.Load += new System.EventHandler(this.Products_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button gyros_xoir_av;
        private System.Windows.Forms.Button gyros_kota_av;
    }
}