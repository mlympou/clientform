﻿namespace ClientForm
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Navigation = new System.Windows.Forms.TableLayoutPanel();
            this.Orders_btn = new System.Windows.Forms.Button();
            this.Customers_btn = new System.Windows.Forms.Button();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.openOrders_grp = new System.Windows.Forms.GroupBox();
            this.OrdersPending = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.ProductsInfo_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.Navigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.openOrders_grp.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Navigation);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1102, 708);
            this.splitContainer1.SplitterDistance = 223;
            this.splitContainer1.TabIndex = 0;
            // 
            // Navigation
            // 
            this.Navigation.ColumnCount = 1;
            this.Navigation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Navigation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Navigation.Controls.Add(this.Orders_btn, 0, 0);
            this.Navigation.Controls.Add(this.Customers_btn, 0, 1);
            this.Navigation.Controls.Add(this.ProductsInfo_btn, 0, 2);
            this.Navigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Navigation.Location = new System.Drawing.Point(0, 0);
            this.Navigation.Name = "Navigation";
            this.Navigation.RowCount = 3;
            this.Navigation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Navigation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Navigation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.Navigation.Size = new System.Drawing.Size(223, 708);
            this.Navigation.TabIndex = 0;
            // 
            // Orders_btn
            // 
            this.Orders_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Orders_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Orders_btn.Location = new System.Drawing.Point(3, 3);
            this.Orders_btn.Name = "Orders_btn";
            this.Orders_btn.Size = new System.Drawing.Size(217, 230);
            this.Orders_btn.TabIndex = 0;
            this.Orders_btn.Text = "Παραγγελίες";
            this.Orders_btn.UseVisualStyleBackColor = true;
            this.Orders_btn.Click += new System.EventHandler(this.Orders_btn_Click);
            // 
            // Customers_btn
            // 
            this.Customers_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Customers_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Customers_btn.Location = new System.Drawing.Point(3, 239);
            this.Customers_btn.Name = "Customers_btn";
            this.Customers_btn.Size = new System.Drawing.Size(217, 230);
            this.Customers_btn.TabIndex = 1;
            this.Customers_btn.Text = "Πελάτες";
            this.Customers_btn.UseVisualStyleBackColor = true;
            this.Customers_btn.Click += new System.EventHandler(this.Customers_btn_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.openOrders_grp);
            this.splitContainer2.Size = new System.Drawing.Size(875, 708);
            this.splitContainer2.SplitterDistance = 325;
            this.splitContainer2.TabIndex = 0;
            // 
            // openOrders_grp
            // 
            this.openOrders_grp.Controls.Add(this.OrdersPending);
            this.openOrders_grp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.openOrders_grp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openOrders_grp.Location = new System.Drawing.Point(0, 0);
            this.openOrders_grp.Name = "openOrders_grp";
            this.openOrders_grp.Size = new System.Drawing.Size(875, 325);
            this.openOrders_grp.TabIndex = 0;
            this.openOrders_grp.TabStop = false;
            this.openOrders_grp.Text = "Νέες Παραγγελίες";
            // 
            // OrdersPending
            // 
            this.OrdersPending.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OrdersPending.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrdersPending.FormattingEnabled = true;
            this.OrdersPending.ItemHeight = 20;
            this.OrdersPending.Location = new System.Drawing.Point(3, 25);
            this.OrdersPending.Name = "OrdersPending";
            this.OrdersPending.Size = new System.Drawing.Size(869, 297);
            this.OrdersPending.TabIndex = 1;
            this.OrdersPending.DoubleClick += new System.EventHandler(this.OrdersPending_DoubleClick);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "New order";
            this.notifyIcon1.BalloonTipTitle = "New order";
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // ProductsInfo_btn
            // 
            this.ProductsInfo_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProductsInfo_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductsInfo_btn.Location = new System.Drawing.Point(3, 475);
            this.ProductsInfo_btn.Name = "ProductsInfo_btn";
            this.ProductsInfo_btn.Size = new System.Drawing.Size(217, 230);
            this.ProductsInfo_btn.TabIndex = 2;
            this.ProductsInfo_btn.Text = "Προϊόντα";
            this.ProductsInfo_btn.UseVisualStyleBackColor = true;
            this.ProductsInfo_btn.Click += new System.EventHandler(this.ProductsInfo_btn_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 708);
            this.Controls.Add(this.splitContainer1);
            this.IsMdiContainer = true;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Delivery POS";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.Navigation.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.openOrders_grp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.ListBox OrdersPending;
        private System.Windows.Forms.GroupBox openOrders_grp;
        private System.Windows.Forms.TableLayoutPanel Navigation;
        private System.Windows.Forms.Button Orders_btn;
        private System.Windows.Forms.Button Customers_btn;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button ProductsInfo_btn;
    }
}

