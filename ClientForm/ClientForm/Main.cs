﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClientForm.Models;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace ClientForm
{
    public partial class Main : Form
    {
        // To support flashing.
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

        //Flash both the window caption and taskbar button.
        //This is equivalent to setting the FLASHW_CAPTION | FLASHW_TRAY flags. 
        public const UInt32 FLASHW_ALL = 3;

        // Flash continuously until the window comes to the foreground. 
        public const UInt32 FLASHW_TIMERNOFG = 12;

        [StructLayout(LayoutKind.Sequential)]
        public struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public UInt32 dwFlags;
            public UInt32 uCount;
            public UInt32 dwTimeout;
        }

        // Do the flashing - this does not involve a raincoat.
        public static bool FlashWindowEx(Form form)
        {
            IntPtr hWnd = form.Handle;
            FLASHWINFO fInfo = new FLASHWINFO();

            fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(fInfo));
            fInfo.hwnd = hWnd;
            fInfo.dwFlags = FLASHW_ALL | FLASHW_TIMERNOFG;
            fInfo.uCount = UInt32.MaxValue;
            fInfo.dwTimeout = 0;

            return FlashWindowEx(ref fInfo);
        }

        public static System.Media.SoundPlayer _player;
        
        public Main()
        {
            InitializeComponent();
            _player = new System.Media.SoundPlayer();
            _player.SoundLocation = @"sounds\ring.wav";        
        }

        private void Main_Load(object sender, EventArgs e)
        {
            var unconfirmedOrders = Dataprovider.GetUnconfirmedOrders();

            foreach (var uncon in unconfirmedOrders)
            {
                Info.Address address = JsonConvert.DeserializeObject<Info.Address>(uncon.Address);
                string[] data = new string[] { uncon.Id.ToString(), $"{address.AddressName} {address.AddressNumber}, {address.Region}" };
                OrdersPending.Items.Add(string.Join(":", data));
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var unconfirmedOrders = Dataprovider.GetUnconfirmedOrders();

            foreach (var uncon in unconfirmedOrders)
            {
                Info.Address address = JsonConvert.DeserializeObject<Info.Address>(uncon.Address);
                string[] data = new string[] { uncon.Id.ToString(), $"{address.AddressName} {address.AddressNumber}, {address.Region}" };
                if (!OrdersPending.Items.Contains(string.Join(":", data)))
                {
                    // new order
                    OrdersPending.Items.Add(string.Join(":", data));

                    // ring
                    _player.PlayLooping();                    

                    // Baloon
                    Thread th1 = new Thread(() => Baloon(address));
                    th1.Start();

                    // Flash
                    FlashWindowEx(this);
                }
            }
        }

        private void Baloon(Info.Address data)
        {
            var notification = new System.Windows.Forms.NotifyIcon()
            {
                Visible = true,
                Icon = System.Drawing.SystemIcons.Information,
                // optional - BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info,
                BalloonTipTitle = "Νέα παραγγελεία απο geyseis.gr!!\n",
                BalloonTipText = $"{data.AddressName} {data.AddressNumber}\n{data.Region}\n{data.NameOnPhone}",
            };

            // Display for 5 seconds.
            notification.ShowBalloonTip(99999999);
            notification.Dispose();
        }

        private void OrdersPending_DoubleClick(object sender, EventArgs e)
        {
            _player.Stop();

            if (OrdersPending.SelectedItem == null)
                return;

            var data = OrdersPending.SelectedItem.ToString();
            int orderId = int.Parse(data.Split(':')[0]);
            Order order = Dataprovider.GetOrderById(orderId);

            OrderShow ordershow = new OrderShow(order, this);
            ordershow.ShowDialog(this);
        }

        private void Orders_btn_Click(object sender, EventArgs e)
        {
            OrdersHistory ordHist = new OrdersHistory();
            ordHist.ShowDialog();
        }

        private void Customers_btn_Click(object sender, EventArgs e)
        {
            Customers cust = new Customers();
            cust.ShowDialog();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            var window = MessageBox.Show(
        "Close the window?",
        "Are you sure?",
        MessageBoxButtons.YesNo);

            e.Cancel = (window == DialogResult.No);
        }

        private void ProductsInfo_btn_Click(object sender, EventArgs e)
        {
            Products p = new Products();
            p.ShowDialog();
        }
    }
}
